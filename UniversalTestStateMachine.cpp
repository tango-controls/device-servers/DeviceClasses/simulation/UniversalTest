/*----- PROTECTED REGION ID(UniversalTestStateMachine.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        UniversalTestStateMachine.cpp
//
// description : State machine file for the UniversalTest class
//
// project :     UniversalTest
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author:  $
// Copyright (C): 2015-2019
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================

#include "UniversalTest.h"

/*----- PROTECTED REGION END -----*/	//	UniversalTest::UniversalTestStateMachine.cpp

//================================================================
//  States  |  Description
//================================================================


namespace UniversalTest_ns
{
//=================================================
//		Attributes Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method     : UniversalTest::is_setState_allowed()
 *	Description: Execution allowed for setState attribute
 */
//--------------------------------------------------------
bool UniversalTest::is_setState_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for setState attribute in Write access.
	/*----- PROTECTED REGION ID(UniversalTest::setStateStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	UniversalTest::setStateStateAllowed_WRITE

	//	Not any excluded states for setState attribute in read access.
	/*----- PROTECTED REGION ID(UniversalTest::setStateStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	UniversalTest::setStateStateAllowed_READ
	return true;
}


//=================================================
//		Commands Allowed Methods
//=================================================


/*----- PROTECTED REGION ID(UniversalTest::UniversalTestStateAllowed.AdditionalMethods) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	UniversalTest::UniversalTestStateAllowed.AdditionalMethods

}	//	End of namespace
